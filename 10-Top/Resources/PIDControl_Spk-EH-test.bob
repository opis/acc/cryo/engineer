<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2025-01-29 11:49:24 by Dominik Domagała-->
<display version="2.0.0">
  <width>2558</width>
  <height>32</height>
  <background_color>
    <color name="Transparent" red="255" green="255" blue="255" alpha="0">
    </color>
  </background_color>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_Gain</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:FB_Gain</pv_name>
    <x>960</x>
    <width>60</width>
    <height>30</height>
    <precision>2</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff_1" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_Gain_1</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_2" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_Gain_2</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_3" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_Gain_3</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
    </rules>
    <tooltip>PID Gain</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_Integration</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:FB_TI</pv_name>
    <x>1034</x>
    <width>80</width>
    <height>30</height>
    <precision>0</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff_1" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_TI_1</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_2" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_TI_2</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_3" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_TI_3</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
    </rules>
    <tooltip>PID Integration Time</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_DeadBand</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:FB_DEADB</pv_name>
    <x>1123</x>
    <width>60</width>
    <height>30</height>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff_1" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_DB_2</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_2" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_DB_1</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_3" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_DB_3</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
    </rules>
    <tooltip>PID Deadband</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_1</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:FB_LMN_HLIM</pv_name>
    <x>1195</x>
    <width>80</width>
    <height>30</height>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff_1" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_HLIM_1</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_2" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_HLIM_2</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_3" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_HLIM_3</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
    </rules>
    <tooltip>FB_LMN_LLIM</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_2</name>
    <pv_name>${SecSub}:SC-PID-${Index}:FB_Setpoint</pv_name>
    <x>80</x>
    <width>65</width>
    <height>30</height>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="Visibilit" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pv0=0">
          <value>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:FB_Setpoint</value>
        </exp>
        <exp bool_exp="pv0=1">
          <value>${SecSub}:${Dis}-${Dev}-${Index}:FB_Setpoint</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:Regulation</pv_name>
      </rule>
      <rule name="bgdiff" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_SP</pv_name>
      </rule>
    </rules>
    <tooltip>FB_Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Action Button_1</name>
    <text></text>
    <x>350</x>
    <width>70</width>
    <height>30</height>
    <background_color>
      <color name="Button_Background" red="236" green="236" blue="236">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions execute_as_one="true">
    </actions>
    <rules>
      <rule name="Opmode" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>Operator</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>PLC</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:OpMode_Manual</pv_name>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:OpMode_Auto</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_3</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:FB_LMN_LLIM</pv_name>
    <x>1282</x>
    <width>80</width>
    <height>30</height>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff_1" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1  &amp;&amp; pvInt2 == 1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_LLIM_1</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_2" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 2">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1  &amp;&amp; pvInt2 == 2">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_LLIM_2</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="bgdiff_3" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1 &amp;&amp; pvInt2 == 3">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1  &amp;&amp; pvInt2 == 3">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(Sec):SC-PID-$(Index):P_Global_LLIM_3</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
    </rules>
    <tooltip>FB_LMN_HLIM</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../../99-Shared/CommonSymbols/error@32.png</symbol>
    </symbols>
    <x>1381</x>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_4</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:LMN</pv_name>
    <x>460</x>
    <width>60</width>
    <height>30</height>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <actions>
    </actions>
    <tooltip>Manipulated Value</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_5</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PV</pv_name>
    <x>559</x>
    <width>35</width>
    <height>30</height>
    <precision>2</precision>
    <show_units>false</show_units>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Proces Value</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_6</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:ProcValueEGU</pv_name>
    <x>594</x>
    <width>40</width>
    <height>30</height>
    <precision>2</precision>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>PROC EGU</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="checkbox" version="2.0.0">
    <name>Check Box_1</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PIDAct</pv_name>
    <label></label>
    <x>20</x>
    <y>6</y>
    <width>31</width>
    <height>16</height>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Action Button_2</name>
    <text>Auto</text>
    <x>1510</x>
    <width>66</width>
    <height>30</height>
    <background_color>
      <color name="Button_Background" red="236" green="236" blue="236">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions execute_as_one="true">
    </actions>
    <rules>
      <rule name="Txtupdate" prop_id="text" out_exp="false">
        <exp bool_exp="(pv0 == 1) ">
          <value>Auto</value>
        </exp>
        <exp bool_exp="(pv1 == 1)">
          <value>Manual</value>
        </exp>
        <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:OpMode_Auto</pv_name>
        <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:OpMode_Manual</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon_1</name>
    <symbols>
      <symbol>../../99-Shared/CommonSymbols/error@32.png</symbol>
    </symbols>
    <x>2350</x>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_8</name>
    <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:FB_Setpoint</pv_name>
    <x>1610</x>
    <width>80</width>
    <height>30</height>
    <precision>2</precision>
    <show_units>false</show_units>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="diff" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>Spk:Cryo-$(CtrlDev)-$(Index):P_Global_SP</pv_name>
      </rule>
    </rules>
    <tooltip>Pressure</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_9</name>
    <x>1787</x>
    <width>80</width>
    <height>30</height>
    <precision>2</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="New Rule" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pvStr0 == &quot;CV&quot;">
          <value>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:ValvePosition</value>
        </exp>
        <exp bool_exp="pvStr0 == &quot;EH&quot;">
          <value>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:HeaterPower</value>
        </exp>
        <pv_name>$(CtrlDev)</pv_name>
      </rule>
    </rules>
    <tooltip>Pressure</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_10</name>
    <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:FB_RampUPRANGE</pv_name>
    <x>2000</x>
    <width>80</width>
    <height>30</height>
    <precision>2</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>Spk:$(CtrlDis)-$(CtrlDev)-$(Index):P_Global_Ramp_UPrange</pv_name>
      </rule>
    </rules>
    <tooltip>Ramping Up Step</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_11</name>
    <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:FB_RampUPTIME</pv_name>
    <x>1910</x>
    <width>80</width>
    <height>30</height>
    <precision>2</precision>
    <show_units>false</show_units>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1">
          <value>
            <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>Spk:$(CtrlDis)-$(CtrlDev)-$(Index):P_Global_Ramp_UPtime</pv_name>
      </rule>
    </rules>
    <tooltip>Ramping Up Time</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_12</name>
    <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:FB_RampDNRANGE</pv_name>
    <x>2180</x>
    <width>80</width>
    <height>30</height>
    <precision>2</precision>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>Spk:$(CtrlDis)-$(CtrlDev)-$(Index):P_Global_Ramp_DNrange</pv_name>
      </rule>
    </rules>
    <tooltip>Ramping Down Step</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>TU_FB_LMN_LLIM_13</name>
    <pv_name>${SecSub}:${CtrlDis}-${CtrlDev}-${Index}:FB_RampDNTIME</pv_name>
    <x>2090</x>
    <width>80</width>
    <height>30</height>
    <precision>2</precision>
    <show_units>false</show_units>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="bgdiff" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == pv1">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; pv1">
          <value>
            <color name="YELLOW" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>Spk:$(CtrlDis)-$(CtrlDev)-$(Index):P_Global_Ramp_DNtime</pv_name>
      </rule>
    </rules>
    <tooltip>Ramping Down Time</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_Profile1</name>
    <text>Profile 1</text>
    <x>731</x>
    <width>60</width>
    <height>30</height>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="BackGround" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == 0 &amp;&amp; pv3 == 1">
          <value>
            <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == 1 &amp;&amp; pv3 == 1">
          <value>
            <color name="GREEN" red="61" green="216" blue="61">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1 == 0 &amp;&amp; pv3 == 2">
          <value>
            <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1 == 1 &amp;&amp; pv3 == 2">
          <value>
            <color name="GREEN" red="61" green="216" blue="61">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv2 == 0 &amp;&amp; pv3 == 3">
          <value>
            <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv2 == 1 &amp;&amp; pv3 == 3">
          <value>
            <color name="GREEN" red="61" green="216" blue="61">
            </color>
          </value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:Profile1</pv_name>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:Profile2</pv_name>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:Profile3</pv_name>
        <pv_name>$(Profile)</pv_name>
      </rule>
      <rule name="profile" prop_id="text" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>Profile 1</value>
        </exp>
        <exp bool_exp="pvInt0 == 2">
          <value>Profile 2</value>
        </exp>
        <exp bool_exp="pvInt0 == 3">
          <value>Profile 3</value>
        </exp>
        <pv_name>$(Profile)</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_Profile1_1</name>
    <text>PID Config</text>
    <x>858</x>
    <width>70</width>
    <height>30</height>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="BackGround" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>
            <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="GREEN" red="61" green="216" blue="61">
            </color>
          </value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:EnablePIDConf</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>btnOpen_26</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>../../99-Shared/Valves/valve analog/faceplates/CV_ControlVALVE_Faceplate_Handler_Update.bob</file>
        <macros>
          <CtrlDev>CV</CtrlDev>
          <CtrlDis>Cryo</CtrlDis>
          <Dev>PID</Dev>
          <Dis>SC</Dis>
          <WIDDEVSecSub>$(SecSub)</WIDDEVSecSub>
          <WIDDEVDis>$(CtrlDis)</WIDDEVDis>
          <WIDDEVDev>$(CtrlDev)</WIDDEVDev>
          <WIDDEVIndex>$(Index)</WIDDEVIndex>
        </macros>
        <target>window</target>
      </action>
    </actions>
    <text>Faceplate</text>
    <x>2270</x>
    <width>70</width>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Action Button_3</name>
    <text></text>
    <x>250</x>
    <width>70</width>
    <height>30</height>
    <background_color>
      <color name="Button_Background" red="236" green="236" blue="236">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions execute_as_one="true">
    </actions>
    <rules>
      <rule name="regmode" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>PID ON</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>PID OFF</value>
        </exp>
        <pv_name>${SecSub}:SC-PID-${Index}:Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
