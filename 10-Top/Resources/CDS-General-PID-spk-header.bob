<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2025-03-07 15:32:13 by Dominik Domagała-->
<display version="2.0.0">
  <name>CDS-General-PID-spk-header</name>
  <macros>
    <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
    <TEST>true</TEST>
    <mDev>TT</mDev>
    <mIndex05>05</mIndex05>
    <mIndex06>06</mIndex06>
    <mIndex60>60</mIndex60>
    <mIndex65>65</mIndex65>
    <Profile>loc://pv-profile2</Profile>
  </macros>
  <width>1395</width>
  <height>130</height>
  <background_color>
    <color red="220" green="225" blue="221" alpha="0">
    </color>
  </background_color>
  <grid_step_x>5</grid_step_x>
  <grid_step_y>5</grid_step_y>
  <widget type="label" version="2.0.0">
    <name>Label_44</name>
    <text>Device Feedback</text>
    <x>445</x>
    <y>33</y>
    <width>75</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_45</name>
    <text>Measured Value</text>
    <x>521</x>
    <y>31</y>
    <width>75</width>
    <height>54</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner_8</name>
    <pv_name>$(Sec):SC-PID-$(Index):P_Global_SP</pv_name>
    <x>175</x>
    <y>100</y>
    <width>80</width>
    <height>25</height>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <tooltip></tooltip>
    <maximum>1000.0</maximum>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_PID_HLIM_22</name>
    <text>Setpoint</text>
    <x>175</x>
    <y>20</y>
    <width>80</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_37</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(Sec):SC-PID-$(Index):Cmd_SP</pv_name>
        <value>1</value>
      </action>
    </actions>
    <text>Execute</text>
    <x>175</x>
    <y>70</y>
    <width>80</width>
    <height>25</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_38</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_Global_Manual</pv_name>
    <text>Operator Control</text>
    <x>355</x>
    <y>100</y>
    <width>80</width>
    <height>25</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_39</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_Global_Auto</pv_name>
    <text>PLC Control</text>
    <x>355</x>
    <y>70</y>
    <width>80</width>
    <height>25</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_94</name>
    <text>PID Profile</text>
    <x>720</x>
    <y>20</y>
    <width>84</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_40</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>loc://pv-profile</pv_name>
    <text>1</text>
    <x>720</x>
    <y>70</y>
    <width>25</width>
    <height>25</height>
    <rules>
      <rule name="en" prop_id="enabled" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pvInt0 != 1">
          <value>true</value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_41</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>2</value>
      </action>
    </actions>
    <pv_name>loc://pv-profile</pv_name>
    <text>2</text>
    <x>749</x>
    <y>70</y>
    <width>25</width>
    <height>25</height>
    <rules>
      <rule name="en" prop_id="enabled" out_exp="false">
        <exp bool_exp="pvInt0 == 2">
          <value>false</value>
        </exp>
        <exp bool_exp="pvInt0 != 2">
          <value>true</value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_42</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>3</value>
      </action>
    </actions>
    <pv_name>loc://pv-profile</pv_name>
    <text>3</text>
    <x>779</x>
    <y>70</y>
    <width>25</width>
    <height>25</height>
    <rules>
      <rule name="en" prop_id="enabled" out_exp="false">
        <exp bool_exp="pvInt0 == 3">
          <value>false</value>
        </exp>
        <exp bool_exp="pvInt0 != 3">
          <value>true</value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_43</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Spk-010$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-010$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-020$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-020$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-030$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-030$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-040$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-040$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-050$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-050$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-060$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-060$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-070$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-070$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-080$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-080$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-090$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-090$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-100$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-100$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-110$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-110$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-120$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-120$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Spk-130$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-130$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>loc://Spk_boolTest-$(mDev)-$(mIndex)</pv_name>
    <text>All</text>
    <x>115</x>
    <y>70</y>
    <width>45</width>
    <height>25</height>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Are your sure you want to confirm all Position Descrepancy Errors?</confirm_message>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_95</name>
    <text>Selector</text>
    <x>105</x>
    <y>20</y>
    <width>65</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner_19</name>
    <pv_name>$(Sec):SC-PID-$(Index):P_Global_Gain_1</pv_name>
    <x>920</x>
    <y>100</y>
    <width>63</width>
    <height>25</height>
    <precision>3</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="pvname" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>$(Sec):SC-PID-$(Index):P_Global_Gain_1</value>
        </exp>
        <exp bool_exp="pvInt0 == 2">
          <value>$(Sec):SC-PID-$(Index):P_Global_Gain_2</value>
        </exp>
        <exp bool_exp="pvInt0 == 3">
          <value>$(Sec):SC-PID-$(Index):P_Global_Gain_3</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <maximum>1000.0</maximum>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner_20</name>
    <pv_name>$(Sec):SC-PID-$(Index):P_Global_TI_1</pv_name>
    <x>995</x>
    <y>100</y>
    <width>80</width>
    <height>25</height>
    <precision>0</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="en" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>$(Sec):SC-PID-$(Index):P_Global_TI_1</value>
        </exp>
        <exp bool_exp="pvInt0 == 2">
          <value>$(Sec):SC-PID-$(Index):P_Global_TI_2</value>
        </exp>
        <exp bool_exp="pvInt0 == 3">
          <value>$(Sec):SC-PID-$(Index):P_Global_TI_3</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <maximum>900000.0</maximum>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner_21</name>
    <pv_name>$(Sec):SC-PID-$(Index):P_Global_DB_1</pv_name>
    <x>1090</x>
    <y>100</y>
    <width>60</width>
    <height>25</height>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="profile" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>$(Sec):SC-PID-$(Index):P_Global_DB_1</value>
        </exp>
        <exp bool_exp="pvInt0 == 2">
          <value>$(Sec):SC-PID-$(Index):P_Global_DB_2</value>
        </exp>
        <exp bool_exp="pvInt0 == 3">
          <value>$(Sec):SC-PID-$(Index):P_Global_DB_3</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <maximum>1000.0</maximum>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_96</name>
    <text>Gain</text>
    <x>919</x>
    <y>20</y>
    <width>60</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_97</name>
    <text>Integral</text>
    <x>995</x>
    <y>20</y>
    <width>80</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_98</name>
    <text>PID DB</text>
    <x>1090</x>
    <y>20</y>
    <width>60</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner_22</name>
    <pv_name>$(Sec):SC-PID-$(Index):P_Global_LLIM_1</pv_name>
    <x>1250</x>
    <y>100</y>
    <width>70</width>
    <height>25</height>
    <precision>2</precision>
    <show_units>true</show_units>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="profile" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>$(Sec):SC-PID-$(Index):P_Global_LLIM_1</value>
        </exp>
        <exp bool_exp="pvInt0 == 2">
          <value>$(Sec):SC-PID-$(Index):P_Global_LLIM_2</value>
        </exp>
        <exp bool_exp="pvInt0 == 3">
          <value>$(Sec):SC-PID-$(Index):P_Global_LLIM_3</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <maximum>1000.0</maximum>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_PID_HLIM_24</name>
    <text>Low Limit</text>
    <x>1250</x>
    <y>20</y>
    <width>70</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_45</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_Global_ReguON</pv_name>
    <text>ON</text>
    <x>270</x>
    <y>70</y>
    <width>70</width>
    <height>25</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_46</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(Sec):Cryo-$(CtrlDev)-$(Index):Cmd_Global_Manual</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_Global_ReguOFF</pv_name>
    <text>OFF</text>
    <x>270</x>
    <y>100</y>
    <width>70</width>
    <height>25</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_44</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Spk-010$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-010$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-020$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-020$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-030$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-030$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-040$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-040$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-050$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-050$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-060$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-060$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-070$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-070$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-080$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-080$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-090$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-090$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-100$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-100$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-110$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-110$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-120$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-120$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
      <action type="write_pv">
        <description>Spk-130$(Sys):SC-PID-${Index}:PIDAct</description>
        <pv_name>Spk-130$(Sys):SC-PID-${Index}:PIDAct</pv_name>
        <value>0</value>
      </action>
    </actions>
    <pv_name>loc://Spk_boolTest-$(mDev)-$(mIndex)</pv_name>
    <text>None</text>
    <x>115</x>
    <y>100</y>
    <width>45</width>
    <height>25</height>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Are your sure you want to confirm all Position Descrepancy Errors?</confirm_message>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>PIDParam_exe_button_1</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_PIDVal_1</pv_name>
    <text>Execute</text>
    <x>1330</x>
    <y>100</y>
    <width>60</width>
    <height>25</height>
    <rules>
      <rule name="vis" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 &gt; 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 &lt;= 1">
          <value>true</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner_18</name>
    <pv_name>$(Sec):SC-PID-$(Index):P_Global_HLIM_1</pv_name>
    <x>1165</x>
    <y>100</y>
    <width>70</width>
    <height>25</height>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="profile" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>$(Sec):SC-PID-$(Index):P_Global_HLIM_1</value>
        </exp>
        <exp bool_exp="pvInt0 == 2">
          <value>$(Sec):SC-PID-$(Index):P_Global_HLIM_2</value>
        </exp>
        <exp bool_exp="pvInt0 == 3">
          <value>$(Sec):SC-PID-$(Index):P_Global_HLIM_3</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <maximum>1000.0</maximum>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_PID_HLIM_23</name>
    <text>High Limit</text>
    <x>1160</x>
    <y>20</y>
    <width>80</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>PID ON button</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_PIDConf_ON</pv_name>
    <text>ON</text>
    <x>820</x>
    <y>70</y>
    <width>85</width>
    <height>25</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>PID OFF Button</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_PIDConf_OFF</pv_name>
    <text>OFF</text>
    <x>820</x>
    <y>100</y>
    <width>85</width>
    <height>25</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_43</name>
    <text>PID Config</text>
    <x>820</x>
    <y>20</y>
    <width>85</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Regulation</text>
    <x>265</x>
    <y>20</y>
    <width>80</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>Op Mode</text>
    <x>355</x>
    <y>20</y>
    <width>80</width>
    <height>50</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_60</name>
    <text>Feedback
Device</text>
    <x>600</x>
    <y>31</y>
    <width>86</width>
    <height>54</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_SelMeasDev2</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(Sec):SC-PID-$(Index):Cmd_Global_SelMeas2</pv_name>
        <value>1</value>
      </action>
    </actions>
    <text>2</text>
    <x>630</x>
    <y>100</y>
    <width>25</width>
    <height>25</height>
    <tooltip>Transmitter 2</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_SelMeasDev3</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(Sec):SC-PID-$(Index):Cmd_Global_SelMeas3</pv_name>
        <value>1</value>
      </action>
    </actions>
    <text>3</text>
    <x>661</x>
    <y>100</y>
    <width>25</width>
    <height>25</height>
    <tooltip>Transmitter 3</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_SelMeasDev1</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(Sec):SC-PID-$(Index):Cmd_Global_SelMeas1</pv_name>
        <value>1</value>
      </action>
    </actions>
    <text>1</text>
    <x>600</x>
    <y>100</y>
    <width>25</width>
    <height>25</height>
    <tooltip>Transmitter 1</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>PIDParam_exe_button_2</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_PIDVal_2</pv_name>
    <text>Execute</text>
    <x>1330</x>
    <y>100</y>
    <width>60</width>
    <height>25</height>
    <visible>false</visible>
    <rules>
      <rule name="Vis" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 2">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 != 2">
          <value>false</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>PIDParam_exe_button_3</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>WritePV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-$(Index):Cmd_PIDVal_3</pv_name>
    <text>Execute</text>
    <x>1330</x>
    <y>100</y>
    <width>60</width>
    <height>25</height>
    <visible>false</visible>
    <rules>
      <rule name="Vis" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 3">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 != 3">
          <value>false</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Activate_PID_2</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-${Index}:Cmd_Global_Profile1</pv_name>
    <text>Activate</text>
    <x>720</x>
    <y>100</y>
    <width>87</width>
    <height>25</height>
    <visible>false</visible>
    <rules>
      <rule name="profile" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 2">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 != 2">
          <value>false</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>Turn P-Action ON</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Activate_PID_3</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-${Index}:Cmd_Global_Profile1</pv_name>
    <text>Activate</text>
    <x>720</x>
    <y>100</y>
    <width>87</width>
    <height>25</height>
    <visible>false</visible>
    <rules>
      <rule name="profile" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 3">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 != 3">
          <value>false</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>Turn P-Action ON</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Activate_PID1_3</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-${Index}:Cmd_Global_Profile1</pv_name>
    <text>Activate</text>
    <x>720</x>
    <y>100</y>
    <width>87</width>
    <height>25</height>
    <rules>
      <rule name="profile" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 &gt; 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 =&lt; 1">
          <value>true</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
      <rule name="profile button name" prop_id="text" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>Activate Profile 1</value>
        </exp>
        <exp bool_exp="pvInt0 == 2">
          <value>Activate Profile 2</value>
        </exp>
        <exp bool_exp="pvInt0 == 3">
          <value>Activate Profile 3</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>Turn P-Action ON</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Activate_PID_4</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-${Index}:Cmd_Global_Profile2</pv_name>
    <text>Activate</text>
    <x>720</x>
    <y>100</y>
    <width>87</width>
    <height>25</height>
    <visible>false</visible>
    <rules>
      <rule name="profile" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 2">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 != 2">
          <value>false</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>Turn P-Action ON</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Activate_PID_5</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <description>Write PV</description>
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
      </action>
    </actions>
    <pv_name>$(Sec):SC-PID-${Index}:Cmd_Global_Profile3</pv_name>
    <text>Activate</text>
    <x>720</x>
    <y>100</y>
    <width>87</width>
    <height>25</height>
    <visible>false</visible>
    <rules>
      <rule name="profile" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 3">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 != 3">
          <value>false</value>
        </exp>
        <pv_name>loc://pv-profile</pv_name>
      </rule>
    </rules>
    <tooltip>Turn P-Action ON</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
</display>
