<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title>Heat Exchanger Temperatures</title>
  <show_toolbar>true</show_toolbar>
  <grid>true</grid>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>true</scroll>
  <start>-1 hours</start>
  <end>now</end>
  <archive_rescale>NONE</archive_rescale>
  <foreground>
    <red>0</red>
    <green>0</green>
    <blue>0</blue>
  </foreground>
  <background>
    <red>255</red>
    <green>255</green>
    <blue>255</blue>
  </background>
  <title_font>Liberation Sans|20|1</title_font>
  <label_font>Liberation Sans|14|1</label_font>
  <scale_font>Liberation Sans|12|0</scale_font>
  <legend_font>Liberation Sans|14|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>Value 1</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>292.637</min>
      <max>294.243</max>
      <grid>true</grid>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>TE-31801</display_name>
      <visible>true</visible>
      <name>CrS-ACCP:CRYO-TT-31801:Val</name>
      <axis>0</axis>
      <color>
        <red>153</red>
        <green>153</green>
        <blue>153</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-03</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-03:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-091</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-91:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>204</red>
        <green>51</green>
        <blue>51</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-095</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-95:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>204</red>
        <green>51</green>
        <blue>51</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-092</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-92:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>127</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-096</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-96:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>128</red>
        <green>153</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-093</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-93:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>128</green>
        <blue>128</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-097</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-97:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>179</green>
        <blue>102</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-094</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-94:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TE-098</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-98:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>128</red>
        <green>0</green>
        <blue>128</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-01</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_CDL):Cryo-TT-01:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>255</green>
        <blue>127</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver01</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>2</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-008</display_name>
      <visible>true</visible>
      <name>$(WIDSecSub_Crm):Cryo-TT-008:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>179</red>
        <green>230</green>
        <blue>179</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>Archiver-linac</name>
        <url>pbraw://archiver-linac-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
  </pvlist>
</databrowser>
