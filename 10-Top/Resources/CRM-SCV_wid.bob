<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-11-18 14:56:46 by emilioasensiconejero-->
<display version="2.0.0">
  <name>Display</name>
  <width>280</width>
  <height>240</height>
  <widget type="group" version="3.0.0">
    <name>BGGrey01</name>
    <width>280</width>
    <height>240</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <widget type="rectangle" version="2.0.0">
      <name>BGGrey01-titlebar</name>
      <width>280</width>
      <height>240</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>BGGrey01-title</name>
      <text>$(SecSub)</text>
      <x>10</x>
      <width>260</width>
      <height>30</height>
      <font>
        <font name="SUBSUB-GROUP-HEADER" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>AnalogTransmitter_BlockIcon_Vacuum_OnlyText_Vertical_6</name>
      <macros>
        <OPI_LOC>../../../</OPI_LOC>
        <PLCName>${PLCName}</PLCName>
        <WIDDev>PT</WIDDev>
        <WIDDis>$(Dis)</WIDDis>
        <WIDIndex>$(Index1)</WIDIndex>
        <WIDLabel>PT-$(Index1)</WIDLabel>
        <WIDSecSub>$(SecSub)</WIDSecSub>
        <DSDis>SC</DSDis>
        <DSDev>FSM</DSDev>
        <DSIndex>010</DSIndex>
      </macros>
      <file>../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitter_BlockIcon_PT_OnlyText_Vertical_Compact_v02.bob</file>
      <x>30</x>
      <y>180</y>
      <width>98</width>
      <height>52</height>
      <resize>1</resize>
      <rules>
        <rule name="selected" prop_id="border_width" out_exp="false">
          <exp bool_exp="pv0 = 0">
            <value>2</value>
          </exp>
          <pv_name>${SecSub}:${DSDis}-${DSDev}-${DSIndex}:DevSelect</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>AnalogTransmitter_BlockIcon_Vacuum_OnlyText_Vertical_7</name>
      <macros>
        <OPI_LOC>../../../</OPI_LOC>
        <PLCName>${PLCName}</PLCName>
        <WIDDev>PT</WIDDev>
        <WIDDis>$(Dis)</WIDDis>
        <WIDIndex>$(Index2)</WIDIndex>
        <WIDLabel>PT-$(Index2)</WIDLabel>
        <WIDSecSub>$(SecSub)</WIDSecSub>
        <DSDis>SC</DSDis>
        <DSDev>FSM</DSDev>
        <DSIndex>010</DSIndex>
      </macros>
      <file>../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitter_BlockIcon_PT_OnlyText_Vertical_Compact_v02.bob</file>
      <x>160</x>
      <y>180</y>
      <width>98</width>
      <height>52</height>
      <resize>1</resize>
      <rules>
        <rule name="selected" prop_id="border_width" out_exp="false">
          <exp bool_exp="pv0 = 1">
            <value>2</value>
          </exp>
          <pv_name>${SecSub}:${DSDis}-${DSDev}-${DSIndex}:DevSelect</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>PV_Valve_BlockIcon_Verical_12</name>
      <macros>
        <OPI_LOC>../../../</OPI_LOC>
        <PLCName>${PLCName}</PLCName>
        <WIDDev>CV</WIDDev>
        <WIDDis>$(Dis)</WIDDis>
        <WIDIndex>$(Index)</WIDIndex>
        <WIDSecSub>${SecSub}</WIDSecSub>
        <WIDSecSub_Crm>${SecSub}</WIDSecSub_Crm>
        <DSDis>SC</DSDis>
        <DSDev>FSM</DSDev>
        <DSIndex>010</DSIndex>
      </macros>
      <file>../../99-Shared/Valves/valve solenoid/blockicons/PV_VALVE_BlockIcon_Vertical_Left_Compact_Crm01.bob</file>
      <x>20</x>
      <y>40</y>
      <width>115</width>
      <height>85</height>
      <resize>1</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_Status_Position</name>
      <text>Valve Position</text>
      <x>160</x>
      <y>40</y>
      <width>110</width>
      <height>25</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="TEXT" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <transparent>false</transparent>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_Opened</name>
      <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:Opened</pv_name>
      <x>160</x>
      <y>65</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </off_color>
      <tooltip>Valve position</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_Opened</name>
      <text>OPENED</text>
      <x>195</x>
      <y>65</y>
      <width>60</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_Closed</name>
      <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:Closed</pv_name>
      <x>160</x>
      <y>90</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </off_color>
      <tooltip>Valve position</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_Closed</name>
      <text>CLOSED</text>
      <x>195</x>
      <y>90</y>
      <width>60</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_Status_Solenoid</name>
      <text>Solenoid</text>
      <x>160</x>
      <y>115</y>
      <width>110</width>
      <height>25</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="TEXT" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <transparent>false</transparent>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_Solenoid</name>
      <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:Solenoid</pv_name>
      <x>160</x>
      <y>140</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="Grid" red="169" green="169" blue="169">
        </color>
      </off_color>
      <tooltip>Valve solenoid state</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_Solenoid</name>
      <text>OUTPUT</text>
      <x>195</x>
      <y>140</y>
      <width>75</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
      <rules>
        <rule name="TextRule" prop_id="text" out_exp="false">
          <exp bool_exp="pv0 == 0">
            <value>OPEN command</value>
          </exp>
          <exp bool_exp="pv0 == 1">
            <value>CLOSE command</value>
          </exp>
          <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:NormallyOpen</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_1</name>
      <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:FB_OpenSP</pv_name>
      <x>30</x>
      <y>150</y>
      <width>98</width>
      <height>25</height>
      <rules>
        <rule name="setpoint" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0 = 0">
            <value>
              <color name="RED" red="252" green="13" blue="27">
              </color>
            </value>
          </exp>
          <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:FB_OpenSP</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_STAT_AUTO_6</name>
      <text>Open Set Point:</text>
      <x>20</x>
      <y>125</y>
      <width>110</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label</name>
      <macros>
        <WIDSecSub>$(SecSub)</WIDSecSub>
        <DSDis>SC</DSDis>
        <DSDev>FSM</DSDev>
        <DSIndex>010</DSIndex>
      </macros>
      <text>No Sensor Selected!</text>
      <x>80</x>
      <y>190</y>
      <width>140</width>
      <height>30</height>
      <visible>false</visible>
      <foreground_color>
        <color name="STOP" red="222" green="33" blue="22">
        </color>
      </foreground_color>
      <background_color>
        <color name="ATTENTION" red="252" green="242" blue="17">
        </color>
      </background_color>
      <transparent>false</transparent>
      <rules>
        <rule name="selected" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 = 2">
            <value>true</value>
          </exp>
          <pv_name>${SecSub}:${DSDis}-${DSDev}-${DSIndex}:DevSelect</pv_name>
        </rule>
      </rules>
    </widget>
  </widget>
</display>
