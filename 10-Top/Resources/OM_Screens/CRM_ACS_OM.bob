<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-10-29 09:16:22 by WojciechBińczyk-->
<display version="2.0.0">
  <name>Crm ACS OM</name>
  <width>250</width>
  <height>100</height>
  <background_color>
    <color red="220" green="225" blue="221" alpha="0">
    </color>
  </background_color>
  <widget type="rectangle" version="2.0.0">
    <name>border</name>
    <width>250</width>
    <height>100</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
    <rules>
      <rule name="Color" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == $(mOMNum) &amp;&amp; pv1 == 1 &amp;&amp; pv2 != 1">
          <value>
            <color name="OK" red="61" green="216" blue="61">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == $(mOMNum) &amp;&amp; pv1 != 1 &amp;&amp; pv2 != 1">
          <value>
            <color name="MINOR" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == $(mOMNum) &amp;&amp; pv2 == 1">
          <value>
            <color name="MAJOR" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 &lt;&gt; $(mOMNum) ">
          <value>
            <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
            </color>
          </value>
        </exp>
        <pv_name>${SecSub_CDL}:SC-FSM-300:OM_ACTIVE_NO</pv_name>
        <pv_name>${SecSub_CDL}:SC-FSM-300:Interlock</pv_name>
        <pv_name>${SecSub_CDL}:SC-FSM-300:Supervision</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>background</name>
    <x>2</x>
    <y>2</y>
    <width>246</width>
    <height>96</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>text_background</name>
    <x>15</x>
    <y>25</y>
    <width>220</width>
    <line_width>0</line_width>
    <line_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </line_color>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>OM_number</name>
    <text>OM0$(mOMNum)</text>
    <width>250</width>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
      </font>
    </font>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>OM_state_name</name>
    <text>$(mOMName)</text>
    <y>22</y>
    <width>250</width>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
      </font>
    </font>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Interlock</name>
    <symbols>
      <symbol>../Spk_OM/CommonSymbols/interlock/Interlock_inactive.svg</symbol>
      <symbol>../Spk_OM/CommonSymbols/interlock/Interlock_yellow.svg</symbol>
    </symbols>
    <x>10</x>
    <y>55</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <rules>
      <rule name="Picture" prop_id="initial_index" out_exp="false">
        <exp bool_exp="pv0 == 1 &amp;&amp; pv1 &lt; $(mOMNumStateMin) || pv1 &gt; $(mOMNumStateMax)">
          <value>0</value>
        </exp>
        <exp bool_exp="pv0 != 1 &amp;&amp; pv1 &gt;= $(mOMNumStateMin) &amp;&amp; pv1 &lt;= $(mOMNumStateMax)">
          <value>1</value>
        </exp>
        <pv_name>${SecSub_CDL}:SC-FSM-300:Interlock</pv_name>
        <pv_name>${SecSub_CDL}:SC-FSM-300:S_ACTIVE_NO</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
    <fallback_symbol>../OM/CommonSymbols/interlock/Interlock_invalid.svg</fallback_symbol>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Interlock</name>
    <text>Interlock</text>
    <x>40</x>
    <y>55</y>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Supervision</name>
    <symbols>
      <symbol>../Spk_OM/CommonSymbols/supervision/supervision_inactive.svg</symbol>
      <symbol>../Spk_OM/CommonSymbols/supervision/supervision_red.svg</symbol>
    </symbols>
    <x>110</x>
    <y>55</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <rules>
      <rule name="Picture" prop_id="initial_index" out_exp="false">
        <exp bool_exp="pv0 == 0 &amp;&amp; pv1 &lt; $(mOMNumStateMin) || pv1 &gt; $(mOMNumStateMax)">
          <value>0</value>
        </exp>
        <exp bool_exp="pv0 == 1 &amp;&amp; pv1 &gt;= $(mOMNumStateMin) &amp;&amp; pv1 &lt;= $(mOMNumStateMax)">
          <value>1</value>
        </exp>
        <pv_name>${SecSub_CDL}:SC-FSM-300:Supervision</pv_name>
        <pv_name>${SecSub_CDL}:SC-FSM-300:S_ACTIVE_NO</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
    <fallback_symbol>../OM/CommonSymbols/supervision/supervision_invalid.svg</fallback_symbol>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Supervision</name>
    <text>Supervision</text>
    <x>140</x>
    <y>55</y>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>VIEW_Action_Button</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>$(mOMNum)</value>
        <description>WritePV</description>
      </action>
    </actions>
    <pv_name>${SecSub_CDL}:SC-FSM-300:OM_SEL_NO-S</pv_name>
    <text>VIEW</text>
    <x>3</x>
    <y>3</y>
    <width>50</width>
    <height>18</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="12.0">
      </font>
    </font>
    <vertical_alignment>0</vertical_alignment>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
