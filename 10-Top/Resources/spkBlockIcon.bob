<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-12-12 11:59:50 by Dominik Domagała-->
<display version="2.0.0">
  <name>Display</name>
  <width>230</width>
  <height>190</height>
  <widget type="rectangle" version="2.0.0">
    <name>bg</name>
    <width>230</width>
    <height>190</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLUE-BORDER" red="47" green="135" blue="148">
      </color>
    </line_color>
    <background_color>
      <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
      </color>
    </background_color>
    <corner_width>20</corner_width>
    <corner_height>20</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>vbName</name>
    <text>${vbName}</text>
    <x>25</x>
    <width>180</width>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>spokeVBs.bob</file>
        <macros>
          <PLCName>${PLCName}</PLCName>
          <SecSub>${SecSub}</SecSub>
          <Spoke_VB_name>${mVboxName}</Spoke_VB_name>
          <WIDSecSub_a>${WIDSecSub_a}</WIDSecSub_a>
          <WIDSecSub_b>${WIDSecSub_b}</WIDSecSub_b>
          <mVboxName>${mVboxName}</mVboxName>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text></text>
    <x>25</x>
    <width>180</width>
    <height>20</height>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>PT60</text>
    <x>10</x>
    <y>20</y>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>TT60</text>
    <x>10</x>
    <y>60</y>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_3</name>
    <text>TT01</text>
    <x>10</x>
    <y>100</y>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_4</name>
    <text>PT01</text>
    <x>120</x>
    <y>20</y>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_5</name>
    <text>TT61</text>
    <x>120</x>
    <y>60</y>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_6</name>
    <text>TT02</text>
    <x>120</x>
    <y>100</y>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_7</name>
    <text>TT03</text>
    <x>10</x>
    <y>140</y>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>${pvPT60}</pv_name>
    <x>10</x>
    <y>40</y>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="threshold_Alarm" prop_id="border_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv3 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 1">
          <value>
            <color name="ORANGE" red="254" green="194" blue="81">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-PT-60:HIHI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-60:HI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-60:LO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-60:LOLO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-60:Warning</pv_name>
      </rule>
      <rule name="Warning" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-PT-60:Warning</pv_name>
      </rule>
    </rules>
    <border_width>2</border_width>
    <border_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_3</name>
    <pv_name>${pvPT01}</pv_name>
    <x>120</x>
    <y>40</y>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="threshold_Alarm" prop_id="border_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv3 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 1">
          <value>
            <color name="ORANGE" red="254" green="194" blue="81">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-PT-01:HIHI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-01:HI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-01:LO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-01:LOLO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-PT-01:Warning</pv_name>
      </rule>
      <rule name="Warning" prop_id="background_color" out_exp="false">
        <pv_name>$(WIDSecSub_a):Cryo-PT-01:Warning</pv_name>
      </rule>
    </rules>
    <border_width>2</border_width>
    <border_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_7</name>
    <pv_name>${pvTT60}</pv_name>
    <x>10</x>
    <y>80</y>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="threshold_Alarm" prop_id="border_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv3 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 1">
          <value>
            <color name="ORANGE" red="254" green="194" blue="81">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-TT-60:HIHI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-60:HI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-60:LO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-60:LOLO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-60:Warning</pv_name>
      </rule>
      <rule name="Warning" prop_id="background_color" out_exp="false">
        <pv_name>$(WIDSecSub_a):Cryo-TT-60:Warning</pv_name>
      </rule>
    </rules>
    <border_width>2</border_width>
    <border_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_8</name>
    <pv_name>${pvTT61}</pv_name>
    <x>120</x>
    <y>80</y>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="threshold_Alarm" prop_id="border_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv3 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-TT-61:HIHI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-61:HI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-61:LO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-61:LOLO</pv_name>
      </rule>
      <rule name="Warning" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="ORANGE" red="254" green="194" blue="81">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-TT-61:Warning</pv_name>
      </rule>
    </rules>
    <border_width>2</border_width>
    <border_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_9</name>
    <pv_name>${pvTT01}</pv_name>
    <x>10</x>
    <y>120</y>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="threshold_Alarm" prop_id="border_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv3 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 1">
          <value>
            <color name="ORANGE" red="254" green="194" blue="81">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-TT-01:HIHI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-01:HI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-01:LO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-01:LOLO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-01:Warning</pv_name>
      </rule>
      <rule name="Warning" prop_id="background_color" out_exp="false">
        <pv_name>$(WIDSecSub_a):Cryo-TT-01:Warning</pv_name>
      </rule>
    </rules>
    <border_width>2</border_width>
    <border_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_10</name>
    <pv_name>${pvTT02}</pv_name>
    <x>120</x>
    <y>120</y>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="threshold_Alarm" prop_id="border_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv3 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 1">
          <value>
            <color name="ORANGE" red="254" green="194" blue="81">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-TT-02:HIHI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-02:HI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-02:LO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-02:LOLO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-02:Warning</pv_name>
      </rule>
      <rule name="Warning" prop_id="background_color" out_exp="false">
        <pv_name>$(WIDSecSub_a):Cryo-TT-02:Warning</pv_name>
      </rule>
    </rules>
    <border_width>2</border_width>
    <border_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_11</name>
    <pv_name>${pvTT03}</pv_name>
    <x>10</x>
    <y>160</y>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="threshold_Alarm" prop_id="border_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv3 == 1">
          <value>
            <color name="RED" red="252" green="13" blue="27">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 1">
          <value>
            <color name="ORANGE" red="254" green="194" blue="81">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv4 == 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDSecSub_a):Cryo-TT-03:HIHI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-03:HI</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-03:LO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-03:LOLO</pv_name>
        <pv_name>$(WIDSecSub_a):Cryo-TT-03:Warning</pv_name>
      </rule>
      <rule name="Warning" prop_id="background_color" out_exp="false">
        <pv_name>$(WIDSecSub_a):Cryo-TT-03:Warning</pv_name>
      </rule>
    </rules>
    <border_width>2</border_width>
    <border_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </border_color>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Symbol</name>
    <pv_name>${WIDSecSub_b}::InterlockMsg</pv_name>
    <symbols>
      <symbol>../../99-Shared/CommonSymbols/interlock_tripped_cms@32.png</symbol>
    </symbols>
    <x>207</x>
    <y>2</y>
    <width>19</width>
    <height>20</height>
    <rules>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pvInt0 ==1">
          <value>true</value>
        </exp>
        <exp bool_exp="pvInt0==0">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub_b}::GroupInterlock</pv_name>
      </rule>
    </rules>
    <tooltip>$(pv_value)</tooltip>
  </widget>
</display>
