<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-04-24 17:35:10 by Dominik Domagała-->
<display version="2.0.0">
  <name>Load</name>
  <width>651</width>
  <height>350</height>
  <widget type="progressbar" version="2.0.0">
    <name>MET_HeaterPower_1</name>
    <pv_name>${WIDSecSub}:${EH1}:HeaterPower</pv_name>
    <x>422</x>
    <y>49</y>
    <width>219</width>
    <height>26</height>
    <rules>
      <rule name="MaxRule" prop_id="maximum" out_exp="true">
        <exp bool_exp="true">
          <expression>pv0</expression>
        </exp>
        <pv_name>${WIDSecSub}:${EH1}:PHiLim</pv_name>
      </rule>
      <rule name="MinRule (duplicate)" prop_id="minimum" out_exp="true">
        <exp bool_exp="true">
          <expression>pv0</expression>
        </exp>
        <pv_name>${WIDSecSub}:${EH1}:PLoLim</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <limits_from_pv>false</limits_from_pv>
    <maximum>500.0</maximum>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Measurement</name>
    <text>Load Connection Status</text>
    <width>185</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Settings</name>
    <text>Load Connection Commands</text>
    <x>10</x>
    <y>131</y>
    <width>221</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Mode</name>
    <text>Single Load Mode</text>
    <x>80</x>
    <y>40</y>
    <width>119</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Mode_1</name>
    <text>Double Load Mode</text>
    <x>80</x>
    <y>80</y>
    <width>126</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="led" version="2.0.0">
    <name>SingleLoadStatus</name>
    <pv_name>${WIDSecSub}:${PSU}:SingleLoad_RB</pv_name>
    <x>40</x>
    <y>40</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <tooltip>One card for one heater (current setup)</tooltip>
  </widget>
  <widget type="led" version="2.0.0">
    <name>DoubleLoad</name>
    <pv_name>${WIDSecSub}:${PSU}:DoubleLoad_RB</pv_name>
    <x>40</x>
    <y>80</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <tooltip>One card for one heater (current setup)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>DoubleLoad</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${EH1}:Cmd_DoubleLoad_ON</pv_name>
    <text>Double Load Command</text>
    <x>30</x>
    <y>290</y>
    <width>200</width>
    <height>33</height>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Heater1Only</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${PSU}:Cmd_Heater1Only_ON</pv_name>
    <text>Heater 1 Only</text>
    <x>30</x>
    <y>190</y>
    <width>201</width>
    <height>33</height>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Heater2Only</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${PSU}:Cmd_Heater2Only_ON</pv_name>
    <text>Heater 2 Only</text>
    <x>30</x>
    <y>223</y>
    <width>201</width>
    <height>33</height>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Measurement_1</name>
    <text>Output Relay Status</text>
    <x>265</x>
    <width>157</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Mode_2</name>
    <text>Relay #1</text>
    <x>305</x>
    <y>40</y>
    <width>56</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Mode_3</name>
    <text>Relay #2</text>
    <x>305</x>
    <y>80</y>
    <width>56</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="led" version="2.0.0">
    <name>SingleLoadStatus_1</name>
    <pv_name>${WIDSecSub}:${PSU}:Relay1_RB</pv_name>
    <x>265</x>
    <y>40</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <tooltip>One card for one heater (current setup)</tooltip>
  </widget>
  <widget type="led" version="2.0.0">
    <name>DoubleLoad_1</name>
    <pv_name>${WIDSecSub}:${PSU}:Relay2_RB</pv_name>
    <x>265</x>
    <y>80</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <tooltip>One card for one heater (current setup)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Measurement_2</name>
    <text>Heater Status</text>
    <x>470</x>
    <width>108</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>${EH1}</text>
    <x>470</x>
    <y>26</y>
    <width>45</width>
    <height>25</height>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>${EH2}</text>
    <x>470</x>
    <y>80</y>
    <width>45</width>
    <height>25</height>
    <auto_size>true</auto_size>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>MET_HeaterPower</name>
    <pv_name>${WIDSecSub}:${EH1}:HeaterPower</pv_name>
    <x>422</x>
    <y>49</y>
    <width>219</width>
    <height>30</height>
    <background_color>
      <color red="255" green="254" blue="253">
      </color>
    </background_color>
    <transparent>true</transparent>
    <precision>3</precision>
    <tooltip>Calculated Heater Power</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="progressbar" version="2.0.0">
    <name>MET_HeaterPower_2</name>
    <pv_name>${WIDSecSub}:${EH2}:HeaterPower</pv_name>
    <x>422</x>
    <y>103</y>
    <width>219</width>
    <height>26</height>
    <rules>
      <rule name="MaxRule" prop_id="maximum" out_exp="true">
        <exp bool_exp="true">
          <expression>pv0</expression>
        </exp>
        <pv_name>${WIDSecSub}:${EH2}:PHiLim</pv_name>
      </rule>
      <rule name="MinRule (duplicate)" prop_id="minimum" out_exp="true">
        <exp bool_exp="true">
          <expression>pv0</expression>
        </exp>
        <pv_name>${WIDSecSub}:${EH2}:PLoLim</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <limits_from_pv>false</limits_from_pv>
    <maximum>500.0</maximum>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>MET_HeaterPower_3</name>
    <pv_name>${WIDSecSub}:${EH2}:HeaterPower</pv_name>
    <x>422</x>
    <y>101</y>
    <width>219</width>
    <height>30</height>
    <background_color>
      <color red="255" green="254" blue="253">
      </color>
    </background_color>
    <transparent>true</transparent>
    <precision>3</precision>
    <tooltip>Calculated Heater Power</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Mode_4</name>
    <text>Single Load Selection</text>
    <x>30</x>
    <y>167</y>
    <width>144</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Mode_5</name>
    <text>Double Load Selection</text>
    <x>30</x>
    <y>270</y>
    <width>151</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
</display>
