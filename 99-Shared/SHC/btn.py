from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil


macros = widget.getEffectiveMacros()

Sec    = macros.getValue("Sec")
Index  = macros.getValue("Index")
CRM_N  = macros.getValue("CRM_N")

CRM_N = int(CRM_N)
Index = int(Index)
widget_name = widget.getPropertyValue("name")


if widget_name == "SwitchFacePlate_Right":
    if not (Sec == "HBL" and CRM_N == 210):
        CRM_N = CRM_N + 10
    if Sec == "Spk" and CRM_N > 130:
        Sec = "MBL"
        CRM_N = 10
    if Sec == "MBL" and CRM_N > 90:
        Sec = "HBL"
        CRM_N = 10
elif widget_name == "SwitchFacePlate_Left":
    if not (Sec == "Spk" and CRM_N == 10):
        CRM_N = CRM_N - 10
    if Sec == "HBL" and CRM_N < 10:
        Sec = "MBL"
        CRM_N = 90
    if Sec == "MBL" and CRM_N < 10:
        Sec = "Spk"
        CRM_N = 130
elif widget_name == "SwitchFacePlate_Up":
    if Index == 91:
        Index = 90
elif widget_name == "SwitchFacePlate_Down":
    if Index == 90:
        Index = 91


CRM_N = str(CRM_N).zfill(3)
Index = str(Index).zfill(3)

map = {}
map["SecSub"] = Sec + "-" + CRM_N + "SHC"
map["CRM_N"] = CRM_N
map["Sec"] = Sec
map["Index"] = Index

ScriptUtil.openDisplay(widget,"PV_VALVE_Faceplate.bob", "REPLACE", map)
