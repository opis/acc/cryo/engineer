<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-01-11 11:50:44 by lhf592-->
<display version="2.0.0">
  <name>PV_VALVE_BlockIcon_1</name>
  <width>88</width>
  <height>75</height>
  <background_color>
    <color red="220" green="225" blue="221" alpha="0">
    </color>
  </background_color>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_1</name>
    <width>88</width>
    <height>75</height>
    <line_width>0</line_width>
    <background_color>
      <color red="112" green="115" blue="114" alpha="60">
      </color>
    </background_color>
    <corner_width>6</corner_width>
    <corner_height>6</corner_height>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupAlarm</pv_name>
    <symbols>
      <symbol>../_Symbols_SVG/CommonSymbols/error/error_inactive.svg</symbol>
      <symbol>../_Symbols_SVG/CommonSymbols/error/error_red.svg</symbol>
    </symbols>
    <y>4</y>
    <width>24</width>
    <height>24</height>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <actions>
    </actions>
    <tooltip>Alarm event occured!</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <fallback_symbol>../../../_Symbols_SVG/CommonSymbols/error/error_invalid.svg</fallback_symbol>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Forced</name>
    <x>3</x>
    <y>28</y>
    <width>18</width>
    <height>18</height>
    <line_width>0</line_width>
    <background_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </background_color>
    <rules>
      <rule name="Visibility rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>A</text>
    <x>5</x>
    <y>24</y>
    <width>15</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>A</value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Auto</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Manual</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupInterlock</pv_name>
    <symbols>
      <symbol>../_Symbols_SVG/CommonSymbols/interlock/Interlock_inactive.svg</symbol>
      <symbol>../_Symbols_SVG/CommonSymbols/interlock/Interlock_yellow.svg</symbol>
    </symbols>
    <y>47</y>
    <width>24</width>
    <height>24</height>
    <visible>false</visible>
    <actions>
    </actions>
    <tooltip>Interlock event occured!</tooltip>
    <fallback_symbol>../../../_Symbols_SVG/CommonSymbols/interlock/Interlock_invalid.svg</fallback_symbol>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>${WIDDev}-${WIDIndex}</text>
    <x>32</x>
    <y>1</y>
    <width>54</width>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate_1</name>
    <actions>
      <action type="open_display">
        <file>PV_VALVE_Faceplate.bob</file>
        <macros>
          <Dev>${Dev}</Dev>
          <Dis>${WIDDis}</Dis>
          <Index>${WIDIndex}</Index>
          <SecSub>${WIDSecSub}</SecSub>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>88</width>
    <height>75</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Valve</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:ValveColor</pv_name>
    <symbols>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/2-Way_Valve_Not-Controlled.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/2-Way_Valve_OK.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/2-Way_Valve_Open.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/2-Way_Valve_Close.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/2-Way_Valve_Close.svg</symbol>
    </symbols>
    <x>31</x>
    <y>25</y>
    <width>40</width>
    <height>40</height>
    <initial_index>4</initial_index>
    <rotation>-90.0</rotation>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <tooltip>Open faceplate</tooltip>
    <fallback_symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/2-Way_Valve_INVALID.svg</fallback_symbol>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Solenoid</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:SolenoidColor</pv_name>
    <symbols>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/Rotary-Piston_Not-Controlled.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/Rotary-Piston_Not-Controlled.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/Rotary-Piston_Not-Controlled.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/Rotary-Piston_OK.svg</symbol>
      <symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/Rotary-Piston_OK.svg</symbol>
    </symbols>
    <x>33</x>
    <y>25</y>
    <width>40</width>
    <height>40</height>
    <rotation>-90.0</rotation>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <tooltip>Open faceplate</tooltip>
    <fallback_symbol>../_Symbols_SVG/2-Way_Valve_Rotary-Piston/Rotary-Piston_INVALID.svg</fallback_symbol>
  </widget>
</display>
