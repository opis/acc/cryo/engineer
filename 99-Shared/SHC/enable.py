from org.csstudio.display.builder.runtime.script import ScriptUtil


SwitchFacePlateRight = ScriptUtil.findWidgetByName(widget, "SwitchFacePlate_Right")
SwitchFacePlateLeft = ScriptUtil.findWidgetByName(widget, "SwitchFacePlate_Left")
SwitchFacePlateUp = ScriptUtil.findWidgetByName(widget, "SwitchFacePlate_Up")
SwitchFacePlateDown = ScriptUtil.findWidgetByName(widget, "SwitchFacePlate_Down")

macros = widget.getEffectiveMacros()


Sec  = macros.getValue("Sec")
Index  = macros.getValue("Index")
CRM_N  = macros.getValue("CRM_N")

if (CRM_N == "210"):
	SwitchFacePlateRight.setPropertyValue("visible", "false")

if (CRM_N == "010" and Sec == "Spk"):
	SwitchFacePlateLeft.setPropertyValue("visible", "false")
    
if (CRM_N == "010" and Sec == "MBL"):
	SwitchFacePlateLeft.setPropertyValue("visible", "false")
    
if (CRM_N == "010" and Sec == "HBL"):
	SwitchFacePlateLeft.setPropertyValue("visible", "false")


if (Index == "091"):
	SwitchFacePlateDown.setPropertyValue("visible", "false")

if (Index == "090"):
	SwitchFacePlateUp.setPropertyValue("visible", "false")