from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

# This script helps displaying the gauges connected to a measurement board
# Expected PVs
# pvs[0]: $(Mod)1_SensorTypeR
# pvs[1]: $(Mod)1_DevNameR
# pvs[2]: $(Mod)2_SensorTypeR
# pvs[3]: $(Mod)2_DevNameR

gauge_1_widget = ScriptUtil.findWidgetByName(widget, "Gauge_1")
gauge_1_blockicon = ScriptUtil.findWidgetByName(widget, "Blockicon_1")
gauge_2_widget = ScriptUtil.findWidgetByName(widget, "Gauge_2")
gauge_2_blockicon = ScriptUtil.findWidgetByName(widget, "Blockicon_2")

gauge_on_disk = {"CC": "vgc", "CM": "vgd", "CP": "vgp", "FC": "vvmc", "PR": "vgp"}
gauge_in_naming = {"vgc": "VGC", "vgd": "VGD", "vgp": "VGP", "vvmc": "VVMC"}
block_icon_path = "../../{type}/blockicons/vac_{type}.bob"

gauge_1_visible = True
gauge_2_visible = True
one_gauge = False

gauge_1_type = PVUtil.getString(pvs[0])
gauge_1_name = PVUtil.getString(pvs[1])
gauge_2_type = PVUtil.getString(pvs[2])
gauge_2_name = PVUtil.getString(pvs[3])

# Hide the gauge 1 if type is "NC" (i.e. Not Connected)
if gauge_1_type == "NC":
    gauge_1_visible = False

# Hide gauge 2 if type is "" or "NC"
#  "" happens for cold cathode gauges since only one of those can be connected to a measurement board
if gauge_2_type == "" or gauge_2_type == "NC":
    gauge_2_visible = False

# Move gauge 1 to center if gauge 2 is ""
if gauge_2_type == "" and gauge_1_type != "NC":
    one_gauge = True


def load_embedded(gwidget, gicon, gvisible, gtype, gname):
    if gvisible:
        macros = gwidget.getEffectiveMacros()
        macros.add("IDX", gname.split("-")[-1])
        macros.add("DEV", gauge_in_naming[gauge_on_disk[gtype]])
        # No need for pipes on VVMC blockicon
        macros.add("PIPE_HORIZONTAL_LEFT", "false")
        macros.add("PIPE_HORIZONTAL_RIGHT", "false")
        gicon.propMacros().setValue(macros)
        gicon.setPropertyValue("file", block_icon_path.format(type = gauge_on_disk[gtype]))
    gwidget.setPropertyValue("visible", gvisible)


# If the gauge type PVs are not INVALID then update the embedded displays
if PVUtil.getSeverity(pvs[0]) != 3 and PVUtil.getSeverity(pvs[2]) != 3:
    load_embedded(gauge_1_widget, gauge_1_blockicon, gauge_1_visible, gauge_1_type, gauge_1_name)
    if one_gauge:
        gauge_1_widget.setPropertyValue("x", 420)
    load_embedded(gauge_2_widget, gauge_2_blockicon, gauge_2_visible, gauge_2_type, gauge_2_name)
