#Generated from VACUUM_VAC-VVA_VAC-VVG.def at 2020-06-11_16:15:17
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg  = ""
code = 0

if PVUtil.getLong(pvs[0]):
    code = PVUtil.getLong(pvs[1])

    msgs = dict({
                 99 : "Open Status & Closed Status Both Active",
                 98 : "Valve Not Closed",
                 97 : "Pressure Interlock No. 1",
                 96 : "Hardware Interlock No. 1",
                 95 : "Pressure Interlock No. 2",
                 94 : "Hardware Interlock No. 2",
                 93 : "Hardware Interlock No. 3",
                 92 : 'Software Interlock (From "External" PLC Function)',
		 87 : "Inrush on Gauge 1 & 2",
                 49 : "Previous Interlock",
                 48 : "Next Interlock",
                 0 : ""
                })

    try:
        msg = msgs[code]
    except KeyError:
        msg = "Error Code: " + PVUtil.getString(pvs[1])
        ScriptUtil.getLogger().severe("Unknown error code {} : {}".format(pvs[1], code))

try:
    pvs[2].setValue(msg)
except:
    if widget.getType() != "action_button":
        widget.setPropertyValue("text", msg)
    widget.setPropertyValue("tooltip", msg)
