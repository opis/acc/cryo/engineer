#Generated from VACUUM_VAC-VVA_VAC-VVG.def at 2020-06-11_16:15:17
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg  = ""
code = 0

if PVUtil.getLong(pvs[0]):
    code = PVUtil.getLong(pvs[1])

    msgs = dict({
                 99 : "Valve Not Open",
                 98 : "Valve Not Closed",
		 16 : "",
                 15 : "Open Interlock Active (Valve Open)",
                 14 : "Pressure Interlock No. 1 Bypassed / Overridden",
                 13 : "Hardware Interlock No. 1 Bypassed / Overridden",
                 12 : "Pressure Interlock No. 2 Bypassed / Overridden",
                 11 : "Hardware Interlock No. 2 Bypassed / Overridden",
                 10 : "Hardware Interlock No. 3 Bypassed / Overridden",
                 9 : "Software Interlock Bypassed / Overridden",
                 8 : "Open Interlock Active (Valve Closed)",
                 7 : "Valve Opening Prevented by Tripped Interlock",
		 6 : "Inrush Gauge 2",
		 5 : "Inrush Gauge 1",
		 4 : "Sensors turned off",
		 3 : "Automatic Open and Close Disabled",
		 2 : "Automatic Open Disabled",
		 1 : "Automatic Close Disabled",
                 0 : ""
                })

    try:
        msg = msgs[code]
    except KeyError:
        msg = "Warning Code: " + PVUtil.getString(pvs[1])
        ScriptUtil.getLogger().severe("Unknown warning code {} : {}".format(pvs[1], code))

try:
    pvs[2].setValue(msg)
except:
    if widget.getType() != "action_button":
        widget.setPropertyValue("text", msg)
    widget.setPropertyValue("tooltip", msg)
