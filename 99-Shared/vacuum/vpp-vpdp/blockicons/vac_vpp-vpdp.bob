<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(vacPREFIX)</name>
  <macros>
    <vacDEV>vpp</vacDEV>
    <vacPREFIX>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</vacPREFIX>
    <vacSYMBOL>../../vmpg/symbols/$(vacDEV)-</vacSYMBOL>
  </macros>
  <width>133</width>
  <height>70</height>
  <scripts>
    <script file="../../COMMON/scripts/isvalid.py" check_connections="false">
      <pv_name>$(vacPREFIX):ValidR</pv_name>
    </script>
  </scripts>
  <widget type="polyline" version="2.0.0">
    <name>PIPE_ABOVE</name>
    <x>46</x>
    <width>50</width>
    <height>3</height>
    <visible>$(PIPE_ABOVE=false)</visible>
    <points>
      <point x="25.0" y="0.0">
      </point>
      <point x="25.0" y="3.0">
      </point>
    </points>
    <line_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </line_color>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>PIPE_LEFT</name>
    <width>48</width>
    <height>50</height>
    <visible>$(PIPE_LEFT=false)</visible>
    <points>
      <point x="0.0" y="25.0">
      </point>
      <point x="48.0" y="25.0">
      </point>
    </points>
    <line_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </line_color>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>PIPE_RIGHT</name>
    <x>94</x>
    <width>39</width>
    <height>50</height>
    <visible>$(PIPE_RIGHT=false)</visible>
    <points>
      <point x="0.0" y="25.0">
      </point>
      <point x="39.0" y="25.0">
      </point>
    </points>
    <line_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </line_color>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>PIPE_BELOW</name>
    <x>46</x>
    <y>48</y>
    <width>50</width>
    <height>22</height>
    <visible>$(PIPE_BELOW=false)</visible>
    <points>
      <point x="25.0" y="0.0">
      </point>
      <point x="25.0" y="22.0">
      </point>
    </points>
    <line_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </line_color>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Symbol</name>
    <pv_name>$(vacPREFIX):StatR</pv_name>
    <symbols>
      <symbol>$(vacSYMBOL)invalid.svg</symbol>
      <symbol>$(vacSYMBOL)acceleration.svg</symbol>
      <symbol>$(vacSYMBOL)nominal-speed.svg</symbol>
      <symbol>$(vacSYMBOL)off.svg</symbol>
      <symbol>$(vacSYMBOL)error.svg</symbol>
    </symbols>
    <x>48</x>
    <y>3</y>
    <width>46</width>
    <height>46</height>
    <rotation>$(ROTATION)</rotation>
    <actions>
      <action type="open_display">
        <file>../../COMMON/LEGEND/legend_pump.bob</file>
        <macros>
          <TITLE>VPP / VPDP - Primary Pump</TITLE>
        </macros>
        <target>standalone</target>
        <description>Open Legend</description>
      </action>
    </actions>
    <rules>
      <rule name="Type" prop_id="tooltip" out_exp="true">
        <exp bool_exp="true">
          <expression>pvStr0 + "\nType: " + pvStr1</expression>
        </exp>
        <pv_name>$(pv_name)</pv_name>
        <pv_name>$(vacPREFIX):ControllerConfigR</pv_name>
      </rule>
    </rules>
    <tooltip>$(pv_value)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Device</name>
    <text>$(DEV)</text>
    <width>40</width>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Index</name>
    <text>$(IDX)</text>
    <y>30</y>
    <width>40</width>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Faceplate</name>
    <actions>
      <action type="open_display">
        <file>../faceplates/vac-vpdp-popup.bob</file>
        <target>window</target>
        <description>Open Faceplate</description>
      </action>
    </actions>
    <text></text>
    <width>46</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Interlock Group</name>
    <x>46</x>
    <y>52</y>
    <width>16</width>
    <height>16</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="symbol" version="2.0.0">
      <name>Interlock_badge</name>
      <pv_name>$(vacPREFIX):ITLckStatR</pv_name>
      <symbols>
        <symbol>../../COMMON/symbols/interlocks/interlock-invalid.svg</symbol>
        <symbol>../../COMMON/symbols/interlocks/interlock-healthy.svg</symbol>
        <symbol>../../COMMON/symbols/interlocks/interlock-tripped.svg</symbol>
        <symbol>../../COMMON/symbols/interlocks/interlock-overridden.svg</symbol>
        <symbol>../../COMMON/symbols/interlocks/interlock-disabled.svg</symbol>
      </symbols>
      <width>16</width>
      <height>16</height>
      <rules>
        <rule name="Visibility" prop_id="visible" out_exp="false">
          <exp bool_exp="pvInt0 == 4">
            <value>false</value>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
      <tooltip>$(pv_value)</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Interlock</name>
      <actions>
        <action type="open_display">
          <file>../../COMMON/faceplates/vac_vpp-vpt-interlocks.bob</file>
          <target>window</target>
          <description>Open interlock details</description>
        </action>
      </actions>
      <pv_name>$(vacPREFIX):ITLckStatR</pv_name>
      <text></text>
      <width>16</width>
      <height>16</height>
      <transparent>true</transparent>
      <rules>
        <rule name="Tooltip" prop_id="tooltip" out_exp="true">
          <exp bool_exp="true">
            <expression>pvStr0</expression>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
      <tooltip>No Interlock</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Badge Locked</name>
    <pv_name>$(vacPREFIX):AutoStartDisStR</pv_name>
    <symbols>
      <symbol></symbol>
      <symbol>../../COMMON/symbols/badges/lock.svg</symbol>
    </symbols>
    <x>99</x>
    <width>16</width>
    <height>16</height>
    <initial_index>1</initial_index>
    <tooltip>Automatic Start Disabled</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Badge Local</name>
    <pv_name>$(vacPREFIX):LocalControlR</pv_name>
    <symbols>
      <symbol>../../COMMON/symbols/badges/local.svg</symbol>
    </symbols>
    <x>116</x>
    <width>16</width>
    <height>16</height>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="true">
        <exp bool_exp="true">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <tooltip>Local Control Enabled</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Badge Started</name>
    <pv_name>$(vacPREFIX):StartDQ-RB</pv_name>
    <symbols>
      <symbol></symbol>
      <symbol>../../COMMON/symbols/badges/started.svg</symbol>
    </symbols>
    <x>99</x>
    <y>34</y>
    <width>16</width>
    <height>16</height>
    <initial_index>1</initial_index>
    <tooltip>Started</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Badge Manual</name>
    <pv_name>$(vacPREFIX):ManualModeR</pv_name>
    <symbols>
      <symbol></symbol>
      <symbol>../../COMMON/symbols/badges/manual.svg</symbol>
    </symbols>
    <x>116</x>
    <y>34</y>
    <width>16</width>
    <height>16</height>
    <initial_index>1</initial_index>
    <tooltip>Manual Control Enabled</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Warning Group</name>
    <x>99</x>
    <y>52</y>
    <width>16</width>
    <height>16</height>
    <style>3</style>
    <transparent>true</transparent>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="true">
        <exp bool_exp="true">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(vacPREFIX):WarningR</pv_name>
      </rule>
    </rules>
    <tooltip>No Warning Message</tooltip>
    <widget type="symbol" version="2.0.0">
      <name>Badge Warning</name>
      <pv_name>$(vacPREFIX):WarningR</pv_name>
      <symbols>
        <symbol></symbol>
        <symbol>../../COMMON/symbols/badges/warning.svg</symbol>
      </symbols>
      <width>16</width>
      <height>16</height>
      <initial_index>1</initial_index>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Warning</name>
      <actions>
        <action type="open_display">
          <file>../../COMMON/faceplates/vac_warning_error.bob</file>
          <macros>
            <WIDGET>vpp-vpdp</WIDGET>
          </macros>
          <target>window</target>
          <description>Open Warning/error details</description>
        </action>
      </actions>
      <pv_name>$(vacPREFIX):WarningR</pv_name>
      <text></text>
      <width>16</width>
      <height>16</height>
      <transparent>true</transparent>
      <scripts>
        <script file="../scripts/warning.py">
          <pv_name>$(vacPREFIX):WarningR</pv_name>
          <pv_name>$(vacPREFIX):WarningCodeR</pv_name>
        </script>
      </scripts>
      <tooltip>No Warning Message</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
</display>
