<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-11-24 11:36:39 by emilioasensiconejero-->
<display version="2.0.0">
  <name>EH_HeaterSupply_PID_Analog_BlockIcon_OnlyText_Horizontal_Compact_Handler_Update</name>
  <width>212</width>
  <height>57</height>
  <background_color>
    <color red="220" green="225" blue="221" alpha="0">
    </color>
  </background_color>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_2</name>
    <x>2</x>
    <y>2</y>
    <width>208</width>
    <height>53</height>
    <line_width>0</line_width>
    <background_color>
      <color red="112" green="115" blue="114" alpha="60">
      </color>
    </background_color>
    <corner_width>6</corner_width>
    <corner_height>6</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Forced</name>
    <x>137</x>
    <y>6</y>
    <width>18</width>
    <height>22</height>
    <line_width>0</line_width>
    <background_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </background_color>
    <rules>
      <rule name="Visibility rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDDEVSecSub}:${WIDDEVDis}-${WIDDEVDev}-${WIDDEVIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupAlarm</pv_name>
    <symbols>
      <symbol>../../_Symbols_SVG/CommonSymbols/error/error_inactive.svg</symbol>
      <symbol>../../_Symbols_SVG/CommonSymbols/error/error_red.svg</symbol>
    </symbols>
    <x>4</x>
    <y>4</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>M</text>
    <x>131</x>
    <y>4</y>
    <width>30</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>A</value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>${WIDDEVSecSub}:${WIDDEVDis}-${WIDDEVDev}-${WIDDEVIndex}:OpMode_Auto</pv_name>
        <pv_name>${WIDDEVSecSub}:${WIDDEVDis}-${WIDDEVDev}-${WIDDEVIndex}:OpMode_Manual</pv_name>
        <pv_name>${WIDDEVSecSub}:${WIDDEVDis}-${WIDDEVDev}-${WIDDEVIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_MV</name>
    <pv_name>${WIDSecSub}:${WIDDEVDis}-${WIDDEVDev}-${WIDDEVIndex}:HeaterPower</pv_name>
    <x>74</x>
    <y>30</y>
    <width>50</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <precision>1</precision>
    <show_units>false</show_units>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <tooltip>Actual PID Output</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP</name>
    <x>6</x>
    <y>30</y>
    <width>50</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <precision>1</precision>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Visibilit" prop_id="pv_name" out_exp="false">
        <exp bool_exp="pv0=0">
          <value>${WIDSecSub}:${WIDDEVDis}-${WIDDEVDev}-${WIDDEVIndex}:FB_Setpoint</value>
        </exp>
        <exp bool_exp="pv0=1">
          <value>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:FB_Setpoint</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>EH-${WIDDEVIndex}</text>
    <x>30</x>
    <y>4</y>
    <width>58</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
      </font>
    </font>
    <background_color>
      <color red="0" green="0" blue="0" alpha="0">
      </color>
    </background_color>
    <transparent>false</transparent>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_PV</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:PV</pv_name>
    <x>142</x>
    <y>30</y>
    <width>50</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <precision>1</precision>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Measured Value</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_W</name>
    <text>W</text>
    <x>124</x>
    <y>30</y>
    <width>14</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <transparent>false</transparent>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>MET_EGU</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:ProcValueEGU</pv_name>
    <x>192</x>
    <y>30</y>
    <width>14</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Engineering Unit</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_EGU_1</name>
    <text>W</text>
    <x>56</x>
    <y>30</y>
    <width>14</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <background_color>
      <color name="Read_Background" red="230" green="235" blue="232">
      </color>
    </background_color>
    <transparent>false</transparent>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="TextRule W" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Regulation</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>../faceplates/EH_HeaterSupply_Analog_Faceplate_Handler_Update.bob</file>
        <macros>
          <DEVDev>$(WIDDEVDev)</DEVDev>
          <DEVDis>$(WIDDEVDis)</DEVDis>
          <DEVIndex>$(WIDDEVIndex)</DEVIndex>
          <DEVSecSub>$(WIDDEVSecSub)</DEVSecSub>
          <Dev>${WIDDev}</Dev>
          <Dis>${WIDDis}</Dis>
          <Index>${WIDIndex}</Index>
          <SecSub>${WIDSecSub}</SecSub>
        </macros>
        <target>window</target>
      </action>
    </actions>
    <text></text>
    <width>206</width>
    <height>28</height>
    <background_color>
      <color red="236" green="236" blue="236" alpha="0">
      </color>
    </background_color>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_REGIcon</name>
    <text>PID</text>
    <x>113</x>
    <y>8</y>
    <width>24</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="12.0">
      </font>
    </font>
    <foreground_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="foreground_color" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>
            <color name="GROUP-BORDER" red="150" green="155" blue="151">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="Text" red="25" green="25" blue="25">
            </color>
          </value>
        </exp>
        <pv_name>${WIDSecSub}:SC-PID-${WIDIndex}:Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>PID Regulation ON indicator</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <symbols>
      <symbol>../../_Symbols_SVG/CommonSymbols/interlock/Interlock_inactive.svg</symbol>
      <symbol>../../_Symbols_SVG/CommonSymbols/interlock/Interlock_yellow.svg</symbol>
    </symbols>
    <x>88</x>
    <y>4</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <rules>
      <rule name="Picture" prop_id="initial_index" out_exp="false">
        <exp bool_exp="pv0 == 0 &amp;&amp; pv1 == 0">
          <value>0</value>
        </exp>
        <exp bool_exp="pv0 == 1 &amp;&amp; pv2 == 1">
          <value>1</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>1</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupInterlock</pv_name>
        <pv_name>${WIDSecSub}:${WIDDEVDis}-${WIDDEVDev}-${WIDIndex}:GroupInterlock</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
    <fallback_symbol>../../_Symbols_SVG/CommonSymbols/interlock/Interlock_invalid.svg</fallback_symbol>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>LBL_EGU_2</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:ProcValueEGU</pv_name>
    <x>56</x>
    <y>30</y>
    <width>14</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="TextRule K" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Regulation</pv_name>
      </rule>
    </rules>
  </widget>
</display>
