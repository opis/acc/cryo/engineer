<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-11-24 09:26:55 by emilioasensiconejero-->
<display version="2.0.0">
  <name>AnalogTransmitter_BlockIcon_OnlyText_Vertical_Compact</name>
  <width>98</width>
  <height>52</height>
  <background_color>
    <color red="255" green="255" blue="255" alpha="0">
    </color>
  </background_color>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_2</name>
    <x>2</x>
    <width>98</width>
    <height>48</height>
    <line_width>0</line_width>
    <background_color>
      <color red="112" green="115" blue="114" alpha="60">
      </color>
    </background_color>
    <corner_width>6</corner_width>
    <corner_height>6</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>OpMode_Forced</name>
    <width>92</width>
    <height>48</height>
    <visible>false</visible>
    <line_width>0</line_width>
    <background_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </background_color>
    <corner_width>6</corner_width>
    <corner_height>6</corner_height>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>${WIDDev}-${WIDIndex}</text>
    <x>22</x>
    <width>68</width>
    <height>29</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="18.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_MeasValue</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:MeasValue</pv_name>
    <x>26</x>
    <y>25</y>
    <width>68</width>
    <height>19</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="18.0">
      </font>
    </font>
    <precision>3</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupAlarm</pv_name>
    <symbols>
      <symbol>../../_Symbols_SVG/CommonSymbols/error/error_inactive.svg</symbol>
      <symbol>../../_Symbols_SVG/CommonSymbols/error/error_red.svg</symbol>
    </symbols>
    <y>1</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>F</text>
    <x>9</x>
    <y>21</y>
    <width>10</width>
    <height>22</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="18.0">
      </font>
    </font>
    <background_color>
      <color name="Transparent" red="255" green="255" blue="255" alpha="0">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value></value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>F</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_FreeRun</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>../faceplates/AnalogTransmitter_Faceplate_LT.bob</file>
        <macros>
          <Dev>${WIDDev}</Dev>
          <Dis>${WIDDis}</Dis>
          <Index>${WIDIndex}</Index>
          <SecSub>${WIDSecSub}</SecSub>
        </macros>
        <target>window</target>
      </action>
    </actions>
    <text></text>
    <width>98</width>
    <height>27</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
</display>
