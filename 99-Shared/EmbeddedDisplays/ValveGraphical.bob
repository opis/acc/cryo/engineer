<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-12-10 13:14:29 by WojciechBińczyk-->
<display version="2.0.0">
  <name>ValveGraphical</name>
  <width>40</width>
  <height>100</height>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <width>40</width>
    <height>100</height>
    <line_width>1</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color red="112" green="115" blue="114" alpha="60">
      </color>
    </background_color>
    <corner_width>3</corner_width>
    <corner_height>3</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>CV</name>
    <y>100</y>
    <width>40</width>
    <height>1</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </background_color>
    <rules>
      <rule name="Color" prop_id="background_color" out_exp="false">
        <exp bool_exp="((pv0 &gt; pv1) || (pv0 &lt; pv2)) &amp;&amp; pv4 == 1">
          <value>
            <color name="ATTENTION" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <exp bool_exp="((pv0 &lt; pv1) || (pv0 &gt; pv2))">
          <value>
            <color name="GRAY-BORDER" red="121" green="121" blue="121">
            </color>
          </value>
        </exp>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):ValvePosition</pv_name>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):FB_LMN_HLIM</pv_name>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):FB_LMN_LLIM</pv_name>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):Regulation</pv_name>
      </rule>
      <rule name="Level" prop_id="height" out_exp="true">
        <exp bool_exp="pvInt0 &gt; 0">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):ValvePosition</pv_name>
      </rule>
      <rule name="Position" prop_id="y" out_exp="true">
        <exp bool_exp="pvInt0 &gt; 0 ">
          <expression>(100-pvInt0)</expression>
        </exp>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):ValvePosition</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>ValvePositionREG</name>
    <y>100</y>
    <width>40</width>
    <height>2</height>
    <visible>false</visible>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="40.0" y="0.0">
      </point>
    </points>
    <line_width>2</line_width>
    <rules>
      <rule name="Position" prop_id="y" out_exp="true">
        <exp bool_exp="pvInt0 &gt; 0">
          <expression>(100-pvInt0)</expression>
        </exp>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):ValvePosition</pv_name>
      </rule>
      <rule name="Color" prop_id="line_color" out_exp="false">
        <exp bool_exp="(pv0 &gt; pv1) || (pv0 &lt; pv2)">
          <value>
            <color name="ATTENTION" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <exp bool_exp="(pv0 &lt; pv1) || (pv0 &gt; pv2)">
          <value>
            <color red="0" green="0" blue="255">
            </color>
          </value>
        </exp>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):ValvePosition</pv_name>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):FB_LMN_HLIM</pv_name>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):FB_LMN_LLIM</pv_name>
      </rule>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):Regulation</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>A</text>
    <x>1</x>
    <width>15</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="14.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>A</value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):OpMode_Auto</pv_name>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):OpMode_Manual</pv_name>
        <pv_name>$(Vbx)$(Sec):Cryo-CV-$(Index):OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_REGIcon</name>
    <text>R</text>
    <x>19</x>
    <width>24</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="14.0">
      </font>
    </font>
    <foreground_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="foreground_color" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>
            <color name="GROUP-BORDER" red="150" green="155" blue="151">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="Text" red="25" green="25" blue="25">
            </color>
          </value>
        </exp>
        <pv_name>$(Vbx)$(Sec):SC-PID-${Index}:Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>PID Regulation ON indicator</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>../Valves/valve analog/faceplates/CV_ControlVALVE_Faceplate_Handler_Update.bob</file>
        <macros>
          <CtrlDev>PID</CtrlDev>
          <CtrlDis>SC</CtrlDis>
          <Dev>PID</Dev>
          <Dis>SC</Dis>
          <Index>${Index}</Index>
          <SecSub>$(Vbx)CDL</SecSub>
          <WIDDev>CV</WIDDev>
        </macros>
        <target>window</target>
      </action>
    </actions>
    <text></text>
    <width>40</width>
    <height>100</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate CV-${Index}</tooltip>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Regulation_Limit_LOW</name>
    <x>20</x>
    <y>100</y>
    <width>20</width>
    <height>2</height>
    <visible>false</visible>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="20.0" y="0.0">
      </point>
    </points>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </line_color>
    <rules>
      <rule name="RegulationLimit" prop_id="y" out_exp="true">
        <exp bool_exp="pv0 &lt;= 98 &amp;&amp; pv0 &gt;= 0">
          <expression>(100-pv0)</expression>
        </exp>
        <exp bool_exp="pv0 &gt; 98">
          <expression>2</expression>
        </exp>
        <exp bool_exp="pv0 &lt; 2">
          <expression>98</expression>
        </exp>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):FB_LMN_LLIM</pv_name>
      </rule>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0 ">
          <value>false</value>
        </exp>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):Regulation</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Regulation_Limit_HI</name>
    <x>20</x>
    <y>100</y>
    <width>20</width>
    <height>2</height>
    <visible>false</visible>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="20.0" y="0.0">
      </point>
    </points>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </line_color>
    <rules>
      <rule name="RegulationLimit" prop_id="y" out_exp="true">
        <exp bool_exp="pv0 &lt;= 98 &amp;&amp; pv0 &gt; 2">
          <expression>(100-pv0)</expression>
        </exp>
        <exp bool_exp="pv0 &gt; 98">
          <expression>2</expression>
        </exp>
        <exp bool_exp="pv0 &lt; 2">
          <expression>98</expression>
        </exp>
        <pv_name>$(Vbx)$(Sec):SC-PID-$(Index):FB_LMN_HLIM</pv_name>
      </rule>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0 ">
          <value>false</value>
        </exp>
        <pv_name>$(Vbx)$(Sec):SC-PID-${Index}:Regulation</pv_name>
      </rule>
    </rules>
  </widget>
</display>
